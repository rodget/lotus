<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => '/category'], function () {
    Route::get('/{slug}', 'Catalog\CategoryController@show')->name('category.show');
    Route::get('/{slug}/{producer}', 'Catalog\CategoryController@show')->name('category.producer');
});

Route::group(['prefix' => '/product'], function () {
    Route::get('/{slug}', 'Catalog\ProductController@show')->name('product.show');
});

Route::group(['middleware' => 'auth', 'prefix' => '/personal'], function () {
    Route::get('/', 'Personal\PersonalController@index')->name('personal');
});

Route::group(['prefix' => '/cart'], function () {
    Route::get('/', 'Cart\CartController@get')->name('cart');
    Route::put('/{product}/add', 'Cart\CartController@add')->name('cart.add');
});

Route::group(['middleware' => 'is.admin', 'prefix' => '/admin'], function () {
    Route::get('/', 'Admin\AdminController@index')->name('admin');

    Route::group(['middleware' => 'permission:manage-category', 'prefix' => '/category'], function () {
        Route::get('/', 'Admin\CategoryController@index')->name('category.index');
        Route::get('/create', 'Admin\CategoryController@create')->name('category.create');
        Route::post('/store', 'Admin\CategoryController@store')->name('category.store');
        Route::get('/edit/{category}', 'Admin\CategoryController@edit')->name('category.edit');
        Route::put('/update/{category}', 'Admin\CategoryController@update')->name('category.update');
        Route::get('/{category}/products', 'Admin\CategoryController@products')->name('category.products');
        Route::get('/{category}/parameters', 'Admin\CategoryController@parameters')->name('category.parameters');
        Route::post('/{category}/parameters', 'Admin\CategoryController@parametersStore')->name('category.parameters.store');
    });

    Route::group(['middleware' => 'permission:manage-product', 'prefix' => '/product'], function () {
        Route::get('/create', 'Admin\ProductController@create')->name('product.create');
        Route::post('/store', 'Admin\ProductController@store')->name('product.store');
        Route::get('/edit/{product}', 'Admin\ProductController@edit')->name('product.edit');
        Route::put('/update/{product}', 'Admin\ProductController@update')->name('product.update');
        Route::get('/parameters/{product}', 'Admin\ProductController@parameters')->name('product.parameters');
        Route::post('/parameters/{product}', 'Admin\ProductController@parametersStore')->name('product.parameters.store');
    });

    Route::group(['middleware' => 'permission:manage-producer', 'prefix' => '/producer'], function () {
        Route::get('/', 'Admin\ProducerController@index')->name('producer.index');
        Route::get('/create', 'Admin\ProducerController@create')->name('producer.create');
        Route::post('/store', 'Admin\ProducerController@store')->name('producer.store');
        Route::get('/edit/{producer}', 'Admin\ProducerController@edit')->name('producer.edit');
        Route::put('/update/{producer}', 'Admin\ProducerController@update')->name('producer.update');
    });

    Route::group(['middleware' => 'permission:manage-parameter', 'prefix' => '/parameter'], function () {
        Route::get('/', 'Admin\ParameterController@index')->name('parameter.index');
        Route::get('/create', 'Admin\ParameterController@create')->name('parameter.create');
        Route::post('/store', 'Admin\ParameterController@store')->name('parameter.store');
        Route::get('/edit/{parameter}', 'Admin\ParameterController@edit')->name('parameter.edit');
        Route::put('/update/{parameter}', 'Admin\ParameterController@update')->name('parameter.update');
    });

    Route::group(['middleware' => 'permission:manage-page', 'prefix' => '/page'], function () {
        Route::get('/', 'Admin\PageController@index')->name('page.index');
        Route::get('/create', 'Admin\PageController@create')->name('page.create');
        Route::post('/store', 'Admin\PageController@store')->name('page.store');
        Route::get('/edit/{page}', 'Admin\PageController@edit')->name('page.edit');
        Route::put('/update/{page}', 'Admin\PageController@update')->name('page.update');
    });
});


/** User authentication */
Route::post('/login', 'Auth\LoginController@login')->name('login-attempt');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

//Auth::routes();

Route::group(['prefix' => '/'], function () {
    Route::get('/{slug}', 'Page\PageController@show')->name('page');
});
