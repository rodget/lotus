<?php

if (!function_exists('activeRoute'))
{
    /**
     * Helper function detects if route from the URL matches passed one
     * @param string $route - URL to check
     * @return bool
     */
    function activeRoute(string $route): bool
    {
        return url()->current() == $route;
    }
}

if (!function_exists('activeRoutes'))
{
    /**
     * Helper function detects if in passed array of routes
     * can be found current url and returns true or false
     * @param array $routes - list of URLs to check
     * @return bool
     */
    function activeRoutes(array $routes = []): bool
    {
        return in_array(url()->current(), $routes);
    }
}

if (!function_exists('russianMonths')) {
    function russianMonths($m)
    {
        $months = [
            null, 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
        ];

        return $months[$m];
    }
}
