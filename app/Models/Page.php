<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class Page extends Model
{
    protected $quarded = ['id'];
    
    protected $fillable = ['slug', 'name', 'text', 'title', 'keywords', 'description', 'status'];
    
    protected static function boot() {
        parent::boot();

        static::creating(function ($page) {
            $page->slug = Str::slug($page->name);
        });
    }
    
    public function scopeBySlug(Builder $query, $slug)
    {
        return $query->where('slug', $slug);
    }
    
    public function scopeByStatus(Builder $query, int $status)
    {
        return $query->where('status', $status);
    }
}
