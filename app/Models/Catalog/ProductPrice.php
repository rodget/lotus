<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $guarded = ['id'];
    
    protected $table = 'products_prices';
}
