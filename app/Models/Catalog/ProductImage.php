<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $guarded = ['id'];
    
    protected $table = 'products_images';
}
