<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class Product extends Model
{
    protected $guarded = ['id']; 
    
    protected static function boot() {
        parent::boot();

        static::creating(function ($product) {
            $product->slug = Str::slug($product->name);
        });
    }
    
    public function category()
    {
        return $this->belongsTo('App\Models\Catalog\Category', 'category_id');
    }
  
    public function price()
    {
        return $this->hasOne('App\Models\Catalog\ProductPrice', 'product_id');
    }
    
    public function image()
    {
        return $this->hasOne('App\Models\Catalog\ProductImage', 'product_id');
    }
    
    public function producer()
    {
        return $this->belongsTo('App\Models\Catalog\Producer', 'producer_id');
    }
    
    public function parameters(ParameterCategory $parameterCategory = null, $fields = [])
    {
        $query = $this->belongsToMany('App\Models\Catalog\Parameter', 'products_parameters', 'product_id', 'parameter_id');
        
        if ($parameterCategory) {
            $query->where('parameters.category_id', $parameterCategory->id);
        }
        
        foreach ($fields as $key => $value) {
            $query->where($key, $value);
        }
        
        return $query->withPivot('value');
    }
    
    public function scopeBySlug(Builder $query, $slug)
    {
        return $query->where('slug', $slug);
    }
    
    public function scopeByStatus(Builder $query, int $status)
    {
        return $query->where('status', $status);
    }
    
    public function scopeByProducerId(Builder $query, int $producerId)
    {
        return $query->where('producer_id', $producerId);
    }
}
