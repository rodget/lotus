<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class ParameterCategory extends Model
{
    protected $quarded = ['id'];
    
    protected $table = 'parameters_categories';
}
