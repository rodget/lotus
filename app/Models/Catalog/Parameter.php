<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Parameter extends Model
{
    protected $quarded = ['id'];
    
    protected $fillable = ['slug', 'name', 'type_id'];
    
    protected static function boot() {
        parent::boot();

        static::creating(function ($parameter) {
            $parameter->slug = Str::slug($parameter->name);
        });
    }
}
