<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class Producer extends Model
{
    protected $guarded = ['id']; 
    
    protected static function boot() {
        parent::boot();

        static::creating(function ($producer) {
            $producer->slug = Str::slug($producer->name);
        });
    }
    
    public function scopeBySlug(Builder $query, $slug)
    {
        return $query->where('slug', $slug);
    }
}
