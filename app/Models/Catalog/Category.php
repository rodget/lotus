<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Catalog\ParameterCategory;

class Category extends Model
{
    protected $quarded = ['id'];
    
    public function products()
    {
        return $this->hasMany('App\Models\Catalog\Product', 'category_id', 'id');
    }
    
    public function childrens() {
        return $this->hasMany(self::class, 'parent_id');
    }
    
    public function parameters(ParameterCategory $parameterCategory = null)
    {
        $query = $this->belongsToMany('App\Models\Catalog\Parameter', 'categories_parameters', 'category_id', 'parameter_id');
        
        if ($parameterCategory) {
            $query->where('parameters.category_id', $parameterCategory->id);
        }
        return $query;
    }
    
    public function scopeBySlug(Builder $query, $slug)
    {
        return $query->where('slug', $slug);
    }
    
    public function scopeByStatus(Builder $query, int $status)
    {
        return $query->where('status', $status);
    }
    
    public function scopeByParent(Builder $query, int $parentId)
    {
        return $query->where('parent_id', $parentId);
    }
}
