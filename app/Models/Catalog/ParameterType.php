<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class ParameterType extends Model
{
    protected $quarded = ['id'];
    
    protected $table = 'parameters_types';
}
