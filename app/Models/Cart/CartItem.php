<?php

namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CartItem extends Model
{
    protected $guarded = ['id'];
    
    protected $table = 'carts_items';
    
    public function scopeByCart(Builder $query, int $cartId)
    {
        return $query->where('cart_id', $cartId);
    }
    
    public function scopeByProduct(Builder $query, int $productId)
    {
        return $query->where('product_id', $productId);
    }
}
