<?php

namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Cart extends Model
{
    protected $guarded = ['id'];
    
    public function items()
    {
        return $this->hasMany('App\Models\Cart\CartItem', 'cart_id');
    }
}
