<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog\Parameter;
use App\Models\Catalog\ParameterType;
use Illuminate\Support\Str;

class ParameterController extends Controller
{
    public function index() {
        return view('admin.parameter.index', [
            'parameters' => Parameter::get()
        ]);
    }

    public function create()
    {
        return view('admin.parameter.create', [
            'parameter' => [],
            'parameterTypes' => ParameterType::get()
        ]);
    }
    
    public function store(Request $request)
    {
        Parameter::create($request->all());
        
        return redirect()->route('parameter.index');
    }
    
    public function edit(Parameter $parameter)
    {
        return view('admin.parameter.edit', [
            'parameter' => $parameter,
            'parameterTypes' => ParameterType::get()
        ]);
    }
    
    public function update(Parameter $parameter, Request $request)
    {
        $parameter->update($request->except(['_method', '_token']));

        return redirect()->route('parameter.index');
    }
}
