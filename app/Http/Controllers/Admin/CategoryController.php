<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog\Category;
use App\Models\Catalog\Parameter;

class CategoryController extends Controller
{
     public function index() {
        return view('admin.category.index', [
            'categories' => Category::get()
        ]);
    }

    public function create()
    {
        return view('admin.category.create', [
            'category' => [],
            'categories' => Category::with('childrens')->where('parent_id', 0)->get(),
            'delimeter' => '',
        ]);
    }
    
    public function store(Request $request)
    {
        Category::create($request->all());
        
        return redirect()->route('category.index');
    }
    
    public function edit(Category $category)
    {
        return view('admin.category.edit', [
            'category' => $category,
            'categories' => Category::with('childrens')->where('parent_id', 0)->get(),
            'delimeter' => '',
        ]);
    }
    
    public function products(Category $category)
    {
        $products = $category->products()->paginate(30);
        
        return view('admin.category.products', [
            'category' => $category,
            'products' => $products,
        ]);
    }
    
    public function parameters(Category $category)
    {
        return view('admin.category.parameters', [
            'category' => $category,
            'parameters' => Parameter::get()
        ]);
    }
    
    public function parametersStore(Category $category, Request $request)
    {
        $category->parameters = json_encode($request->get('parameters'));
        $category->save();
        
        $category->parameters()->sync($request->get('parameters'));
        
        return redirect()->route('category.index');
    }
}
