<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Catalog\Producer;

class ProducerController extends Controller
{
    public function index() {
        return view('admin.producer.index', [
            'producers' => Producer::get()
        ]);
    }

    public function create()
    {
        return view('admin.producer.create', [
            'producer' => [],
        ]);
    }
    
    public function store(Request $request)
    {
        $file = $request->file('file');

        if ($file) {
            $fileName = substr(md5(microtime() . rand(0, 1000)), 0, 16) . '.' . $file->getClientOriginalExtension();
            $file->move(storage_path('app/public/producers'), $fileName);

            $request['logo'] = $fileName;
        }
        
        Producer::create($request->except('file'));
        
        return redirect()->route('producer.index');
    }
    
    public function edit(Producer $producer)
    {
        return view('admin.producer.edit', [
            'producer' => $producer,
        ]);
    }
    
    public function update(Producer $producer, Request $request)
    {
        $file = $request->file('file');

        if ($file) {
            if ($producer->logo && Storage::exists('public/producers/' . $producer->logo)) {
                Storage::delete('public/producers/' . $producer->logo);
            }

            $fileName = substr(md5(microtime() . rand(0, 1000)), 0, 16) . '.' . $file->getClientOriginalExtension();
            $file->move(storage_path('app/public/producers'), $fileName);

            $request['logo'] = $fileName;
        }

        $producer->update($request->except('file'));

        return redirect()->route('producer.index');
    }
}
