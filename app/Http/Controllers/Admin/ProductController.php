<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\Catalog\Category;
use App\Models\Catalog\Producer;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductPrice;
use App\Models\Catalog\ProductImage;

class ProductController extends Controller
{
    public function create()
    {
        return view('admin.product.create', [
            'producers' => Producer::get(),
            'categories' => Category::with('childrens')->where('parent_id', 0)->get(),
            'delimeter' => '',
        ]);
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:products,name',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $product = Product::create($request->except(['_token', 'price', 'file']));
        
        if (!empty($product)) {
            ProductPrice::create(['product_id' => $product->id, 'price' => $request->get('price') ?? 0]);
        }
        
        return redirect()->route('category.products', Category::find($request->get('category_id')));
    }
    
    public function edit(Product $product)
    {
        return view('admin.product.edit', [
            'product' => $product,
            'producers' => Producer::get(),
            'categories' => Category::with('childrens')->where('parent_id', 0)->get(),
            'delimeter' => '',
        ]);
    }
    
    public function update(Product $product, Request $request)
    {
        $file = $request->file('file');

        if ($file) {
            if ($product->image && Storage::exists('public/products/' . $product->image->path)) {
                Storage::delete('public/products/' . $product->image->path);
            }

            $fileName = substr(md5(microtime() . rand(0, 1000)), 0, 16) . '.' . $file->getClientOriginalExtension();
            $file->move(storage_path('app/public/products'), $fileName);
            
            ProductImage::updateOrCreate(['product_id' => $product->id], ['path' => $fileName]);
        }

        $product->update($request->except(['file', 'price']));
        
        ProductPrice::updateOrCreate(['product_id' => $product->id], ['price' => $request->get('price') ?? 0]);

        return redirect()->route('category.products', Category::find($request->get('category_id')));
    }
    
    public function parameters(Product $product)
    {
        return view('admin.product.parameters', [
            'product' => $product,
            'category' => $product->category,
        ]);
    }
    
    public function parametersStore(Product $product, Request $request)
    {
        $result = [];
        
        foreach ($request->get('parameters') as $key => $item) {
            if ($item) {
                $result[$key]['value'] = $item;
            }
        }

        $product->parameters()->sync($result);
        
        return redirect()->route('category.products', Category::find($product->category_id));
    }
}
