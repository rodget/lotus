<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
    public function index() {
        return view('admin.page.index', [
            'page' => Page::get()
        ]);
    }

    public function create()
    {
        return view('admin.page.create', [
            'page' => [],
        ]);
    }
    
    public function store(Request $request)
    {
        $page = Page::create($request->except('_token'));
        
        return redirect()->route('page.index');
    }
    
    public function edit(Page $page)
    {
        return view('admin.page.edit', [
            'page' => $page,
        ]);
    }
    
    public function update(Page $page, Request $request)
    {
        $page->update($request->except('_token'));

        return redirect()->route('page.index');
    }
}
