<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
    public function show($slug)
    {
        $page = Page::BySlug($slug)
                ->ByStatus(1)
                ->first();
                
        return view('pages.show', [
            'page' => $page,
        ]);
    }
}
