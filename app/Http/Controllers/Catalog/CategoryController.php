<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog\Category;
use App\Models\Catalog\Product;
use App\Models\Catalog\Producer;

class CategoryController extends Controller
{    
    public function show($slug, $producerSlug = '')
    {
        $producer = $producerSlug ? Producer::bySlug($producerSlug)->first() : null;

        $category = Category::BySlug($slug)
                ->with(['products' => function($query) use ($producer) {
                    if (!empty($producer)) {
                        //$query->byProducerId($producer->id);
                    }
                    $query->with('price');
                }])->ByStatus(1)
                ->first();

        $query = Product::where('category_id', $category->id)->ByStatus(1);
        if (!empty($producer)) {
            $query->byProducerId($producer->id);
        }
        $products = $query->paginate(30);
        
        $producers = Producer::select('producers.*')
                ->join('products', 'producers.id', '=', 'products.producer_id')
                ->where('products.category_id', $category->id)
                ->groupBy('producers.id')
                ->get();

        $page = (object)[
            'meta_title' => $category->title,
            'meta_description' => $category->description,
            'meta_keywords' => $category->keywords,
        ];
                
        return view('category.show', [
            'page' => $page,
            'categories' => Category::ByParent($category->id)->get(),
            'category' => $category,
            'producers' => $producers,
            'producer' => $producer,
            'products' => $products,
        ]);
    }
}
