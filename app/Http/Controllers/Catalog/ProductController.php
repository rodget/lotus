<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog\Product;

class ProductController extends Controller
{
    public function show($slug)
    {
        $product = Product::BySlug($slug)
                ->ByStatus(1)
                ->first();

        $page = (object)[
            'meta_title' => $product->title,
            'meta_description' => $product->description,
            'meta_keywords' => $product->keywords,
        ];
        
        return view('product.show', [
            'page' => $page,
            'product' => $product,
        ]);
    }
}
