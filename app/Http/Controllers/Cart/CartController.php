<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog\Product;
use App\Models\Cart\Cart;
use App\Models\Cart\CartItem;

class CartController extends Controller
{
    /**
     * Get Cart id from the session
     * @return int
     * @throws GuzzleException
     */
    public function getCartId(): int
    {
        if (!session()->has('cartId')) {
            $cartId = Cart::create()->id;

            if ($cartId) {
                session(['cartId' => $cartId]);
            }
        }

        return session()->get('cartId');
    }

    public function get()
    {
        return [
            'status' => 'ok',
            'cart' => $this->getCart(),
        ];
    }
    
    public function add(Product $product, Request $request)
    {
        $cartId = $this->getCartId();

        if ($cartItem = CartItem::ByCart($cartId)->byProduct($product->id)->first()) {
            $cartItem->count += $request->get('count');
            $cartItem->save();
        } else {
            $cartItem = CartItem::create([
                        'cart_id' => $cartId,
                        'product_id' => $product->id,
                        'count' => $request->get('count'),
                        'price' => $product->price->price,
            ]);
        }
        
        return [
            'status' => 'ok',
            'cart' => $this->getCart(),
        ];
    }
    
    public function getCart()
    {
        $cartCount = 0;
        $cartPrice = 0;
        $cartItems = [];
        
        if (session()->has('cartId')) {
            $cart = Cart::find(session()->get('cartId'));
            
            $cartItems = $cart->items;

            foreach ($cartItems as $cartItem) {
                $cartCount += $cartItem->count;
                $cartPrice += $cartItem->count * $cartItem->price;
            }
        }
        
        return (object)[
            'count' => $cartCount,
            'price' => $cartPrice,
            'items' => $cartItems,
        ];
    }
}
