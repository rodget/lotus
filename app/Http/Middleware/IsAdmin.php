<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()) {
            abort(403);
        }
        if(!auth()->user()->hasRole('administrator') &&  !auth()->user()->hasRole('project-manager') &&  !auth()->user()->hasRole('web-developer')) {
            abort(403);
        }
        
        return $next($request);
    }
}
