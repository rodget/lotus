<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Catalog\Category;
use App\Http\Controllers\Cart\CartController;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->categoriesMenu();
        
        $this->cartMenu();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    
    // Top and Bottom categories menu
    public function categoriesMenu()
    {
        View::composer('layouts.footer', function($view) {
            $view->with('categories', Category::where('parent_id', 0)->where('status', 1)->get());
        });
        
        View::composer('layouts.modals.menu', function($view) {
            $view->with('categories', Category::where('parent_id', 0)->where('status', 1)->get());
        });
        
    }
    
    public function cartMenu()
    {
        View::composer('layouts.partials.header', function($view) {
            $view->with('cart', (new CartController)->getCart());
        });
    }
}
