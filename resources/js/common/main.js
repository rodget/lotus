import wNumb from 'wNumb';
import noUiSlider from 'nouislider';

$(document).ready(function () {
    var sliderPrice = document.getElementById('sliderPrice');
    var sliderArea = document.getElementById('sliderArea');

    if (sliderPrice) {
        noUiSlider.create(sliderPrice, {
            start: [0, 200000],
            connect: true,
            step: 500,
            range: {
                'min': 0,
                'max': 200000
            },
            tooltips: [true, true],
            format: wNumb({
                decimals: 0
            }),
        });
    }
    
    if (sliderArea) {
        noUiSlider.create(sliderArea, {
            start: [0, 100],
            connect: true,
            step: 1,
            range: {
                'min': 0,
                'max': 100
            },
            tooltips: [true, true],
            format: wNumb({
                decimals: 0
            }),
        });
    }
});
