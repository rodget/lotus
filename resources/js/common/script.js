$(document).ready(function () {
    // Попапы
    $.fancybox.defaults.closeExisting = true;
    $.fancybox.defaults.transitionEffect = 'fade';
    $.fancybox.defaults.transitionDuration = 500;
    $.fancybox.defaults.loop = true;
    $.fancybox.defaults.autoFocus = false;
    $.fancybox.defaults.backFocus = false;
    $.fancybox.defaults.touch = false;
    $.fancybox.defaults.buttons = [
        'close'
    ];
    
    // Табы
    var transitionActive = false;

    $('body').on('click', '.js-tab-open', function (e) {

        e.preventDefault();

        var $this = $(this),
            $parent = $this.parent(),
            tab_connector = $this.closest('[data-tabs]').data('tabs'),
            $tab_item = $('[data-tabs-content="' + tab_connector + '"]'),
            $link = $this.attr('href'),
            height = $($link).outerHeight(),
            height_initial = $tab_item.outerHeight(),
            transitionDuration = 500;

        if ($parent.hasClass('active') || (transitionActive === true)) {
            return false;
        }

        $parent.addClass('active').siblings().removeClass('active');
        $($link).addClass('active').siblings().removeClass('active');
        $tab_item.css({
            'height': height_initial
        });

        setTimeout(function () {
            $tab_item.css({
                'height': height
            });
        }, 20);

        transitionActive = true;

        setTimeout(function () {
            $tab_item.css({
                'height': 'auto'
            });
            transitionActive = false;
        }, transitionDuration);

        // Отложенная загрузка карты метро
        var $interact_lazy = $('.interact-lazy');
        if ($($link).find('.interact-lazy').length) {
            var interact_src = $interact_lazy.data('src');
            $interact_lazy.attr('src', interact_src);
            $interact_lazy.on('load', function () {
                interact_reinit();
            });
        }

        // Закрытие фильтра при переключении табов
        if ($this.closest('.tabs-pharmacy').length && (window_width < 600)) {
            $('.filters-open-wrap').removeClass('active');
            $('.js-open-filter').find('.text').text('Развернуть фильтр');
            if ($('.pharmacy-filters').hasClass('visible')) {
                $('.pharmacy-filters').hide().removeClass('visible');
            }
            $('.pharmacy-filters.-mobile').show();

        }

    });
    
    // Открыть меню
    $('body').on('click', '.js-open-menu', function (e) {
        e.preventDefault();

        var $this = $(this);

        if (!$this.hasClass('active')) {
            $this.addClass('active');
            $.fancybox.open({
                src: '#pop-menu-categories',
                type: 'inline',
                arrows: false,
                infobar: false,
                toolbar: false,
                beforeShow: function (instance, slide) {
                    $('body').addClass('fancybox-menu');
                    $('.fancybox-container').addClass('fancybox-menu');
                },
                beforeClose: function () {
                    $('body').removeClass('fancybox-menu');
                    $('.open-menu').removeClass('active');
                },
            });
        } else {
            $this.removeClass('active');
            $.fancybox.close();
        }
    });
    
    // Открыть поиск
    $('body').on('click', '.js-open-search', function (e) {
        e.preventDefault();

        var $this = $(this),
            $parent = $this.closest('.form-search');

        if (!$parent.hasClass('active')) {
            $parent.addClass('active');
            setTimeout(function () {
                $parent.find('input[type="search"]').focus();
            }, 300);
        } else {
            $parent.removeClass('active');
        }

    });

    // Закрыть поиск
    $('body').on('click', '.js-close-search', function (e) {
        e.preventDefault();

        var $this = $(this),
            $parent = $this.closest('.form-search');

        $parent.removeClass('active');

    });
    
    // Развернуть описание
    $('body').on('click', '.js-show-descr', function (e) {
        e.preventDefault();

        var $this = $(this),
            $parent = $this.closest('.descr');

        $parent.addClass('active');
        $this.remove();

    });
    
    // Раскрыть аккордион
    $('body').on('click', '.js-open-accordion', function (e) {
        e.preventDefault();

        var $this = $(this),
            $parent = $this.closest('.accordion-item');

        if (!$parent.hasClass('opened')) {
            $parent.addClass('opened');
            $parent.find('.hidden').slideDown(300);
        } else {
            $parent.removeClass('opened');
            $parent.find('.hidden').slideUp(300);
        }

    });
    
    // Поле select
    $('.js-select').each(function () {

        var $this = $(this),
            $parent = $this.parent();

        $this.select2({
            language: "ru",
            width: '100%',
            dropdownParent: $parent,
            minimumResultsForSearch: 10
        });

        $this.next('.select2').find('.select2-selection__arrow').append('<svg viewBox="0 0 13 24" xmlns="http://www.w3.org/2000/svg"><path d="m3 12.06 9.5 9.7a1.34 1.34 0 0 1 0 1.86 1.27 1.27 0 0 1 -1.82 0l-10.3-10.54a1.33 1.33 0 0 1 -.23-1.54 1.31 1.31 0 0 1 .34-.62l10.31-10.54a1.27 1.27 0 0 1 1.82 0 1.32 1.32 0 0 1 0 1.86z" fill-rule="evenodd"/></svg>');

        $this.on('select2:open', function () {
            var $dropdown = $('.select2-dropdown');
            setTimeout(function () {
                $dropdown.addClass('animate');
            }, 10);
            setTimeout(function () {
                $dropdown.find('input[type="search"]').focus();
            }, 200);

        });

        $this.on('select2:closing', function () {
            var $dropdown = $('.select2-dropdown');
            $dropdown.removeClass('animate');
        });

    });
});
