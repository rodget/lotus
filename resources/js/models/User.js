import Form from './Form';

export default class User extends Form {
    /**
     * logout User
     * @returns {Promise<unknown>}
     */
    logout()
    {
        const thisClass = this;

        return new Promise((resolve, reject) => {
            thisClass.submit('post', '/users/logout').then(async response => {
                if (response)
                {
                    resolve(response);
                } else
                {
                    reject(response);
                }
            });
        });
    }
    
    auth()
    {
        $.fancybox.open({
            src: '#pop-auth',
            type: 'inline',
            beforeLoad: function () {
                $("#auth__login").removeClass('error');
                $("#auth__password").removeClass('error');
                $("#auth__login-error").html('Неверный email');
            },
        });
    }
    
    doAuth(login, password)
    {
        const thisClass = this;

        return new Promise((resolve, reject) => {
            thisClass.submit('post', '/login', {email: login, password: password}).then(async response => {
                if (response)
                {
                    resolve(response);
                } else
                {
                    reject(response);
                }
            });
        });
    }
}
