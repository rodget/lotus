import Form from './Form';

export default class Cart extends Form {
    get(url)
    {
        const thisClass = this;

        return new Promise((resolve, reject) => {
            thisClass.submit('get', url).then(response => {
                if (response.status == 'ok')
                {
                    resolve(response.cart);
                } else {
                    reject(response);
                }
            });
        });
    }
    
    add(url, parameters)
    {
        const thisClass = this;

        return new Promise((resolve, reject) => {
            thisClass.submit('put', url, parameters).then(response => {
                if (response.status == 'ok')
                {
                    resolve(response);
                } else {
                    reject(response);
                }
            });
        });
    }
}
