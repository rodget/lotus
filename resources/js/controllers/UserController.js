import User from '../models/User';

export default class UserController {
    constructor()
    {
        this.user = new User();
        this.addEventListeners(); // add events to dropdowns (select), checkboxes, buttons
    }

    /**
     * Add events when page loads
     * to user some buttons
     */
    addEventListeners()
    {
        const thisClass = this;
        
        $('body').on('click', '[data-click="auth"]', function (e) {
            e.preventDefault();
            
            thisClass.user.auth();
        }).on('click', '[data-click="doAuth"]', function (e) {
            e.preventDefault();
            
            const login = $("#auth__login").val();
            const password = $("#auth__password").val();
            
            thisClass.user.doAuth(login, password).then(response => {
                if (response.status == 'error') {
                    $("#auth__login").addClass('error');
                    $("#auth__password").addClass('error');
                } else if (response.status == 'ok') {
                    location.reload();
                }
            }).catch(response => {
                $("#auth__login-error").html('Произошла ошибка! Повторите попытку.');
                $("#auth__login").addClass('error');
            });
        });
    }
}

const userController = new UserController();
