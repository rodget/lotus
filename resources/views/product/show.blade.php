@extends('layouts.index')

@section('breadcrumbs')
    {{ Breadcrumbs::render('product', $product) }}
@endsection

@section('content')
<div class="container product">
    <div class="row mt-4">
        <div class="col-12">
            <h1 class="h1 product-header">{{ $product->name }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 mt-4">
            <div class="product-image">
                @if ($product->image)
                <img src="/storage/products/{{ $product->image->path }}" class="d-block w-100" />
                @else
                <img src="/storage/images/no_image.jpg" class="d-block w-100" />
                @endif
            </div>
        </div>
        <div class="col-12 col-md-6 mt-4">
            <div class="d-flex justify-content-end product-code"><span class="bg-lotus-blue text-white mr-1 px-1">Код товара:</span>{{ $product->id }}</div>
            <div class="row">
                <div class="col-6 d-flex align-items-center product-price">
                    {{ $product->price ? number_format($product->price->price, 0, '.', ' ') : ''}} р.
                </div>
                <div class="col-6 d-flex align-items-center justify-content-end">
                    @if ($product->producer->logo) 
                        <img src="/storage/producers/{{ $product->producer->logo }}" />
                    @endif
                </div>
            </div>
            <hr class="w-100 my-0" />
            <div class="row mt-4">
                <div class="col-12">
                    <div class="product-info__Item">Производитель: <span>{{ $product->producer->name }}</span></div>
                    <div class="product-info__Item">Модель: <span>{{ $product->name }}</span></div>
                    <div class="product-info__Item">Наличие: <span>уточняйте</span></div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12">
                    <reservation ref="reservation" product_id="{{ $product->id }}"></reservation>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12">
                    <div class="product-support d-flex p-4 align-items-center">
                        <div class="product-support__Left">
                            <div class="product-support__Title">
                                +7 (495) 221 77 27
                            </div>
                            <div class="product-support__Text mt-2">
                                Консультации по любым вопросам покупки и дальнейшего обслуживания техники
                            </div>
                        </div>
                        <div class="product-support__Right">
                            <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <circle cx="30" cy="30" r="29.5" fill="#F2F2F2" stroke="black"/>
                            <rect x="15" y="15" width="30" height="30" fill="url(#patternPhone)"/>
                            <defs>
                            <pattern id="patternPhone" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#imagePhone" transform="scale(0.0333333)"/>
                            </pattern>
                            <image id="imagePhone" width="30" height="30" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAdUSURBVHjanFcLUI5ZGH77+0NSKkJtQoRxzyWMy7CyySUs1lomt+w2uS0ru8aQy5osy8oYzDBK2NJoZQejtk0uKddal0KlXNZkKouISrXP882c5vPNP7+dPTOn7/vPd857fd7nPdnU1NTIu3fvpKioSGxsbOTOnTsybNgw8fDwkPr6euHgekVFheTl5WnvavDdZDJJeHh43NmzZwOxv5Gdnd3bNm3aPOzZs+cFPz+/I127dr1CWdzbv39/adKkiSbXrIQ0atRIXr165Xrq1KlgLy+vZGzOE92ggjdv3khdXV2Dcq7hnA2M8sK7Y7NmzZ7BEYcnT574Pn782Pf06dNLoPz3ZcuWfde5c+cCvTyhx2/fvpWcnBynAQMG/Iml+nbt2hWdO3fOh5ZRkZr8rZ+VlZVSXl4uKSkpDqtXr24dEhLSdNWqVS4RERH9ZsyYsa5ly5Z/U56Li0v5zp07P1PnNOP5h8oDAgLS0tPTRyqD2rZtWwSBAQhVvgq5Cq8aZWVlcuTIES18HK9fv5bmzZuLo6OjODg4SFVVlfOhQ4d+OXHixJzGjRu/g7zA4cOHp2uKYbHD0aNHvyotLbXftGnT9urqalsluFOnTvmxsbEBgwcPLtJ7qVJz9+5dz5UrV85HijxsbW1toPRJnz590oYMGZIJxbVYEycnJzl48ODmbdu2fe/t7V184cIFP0SgVA4fPuzPcCxdunRjVFTUPLzX8beaHTt2zCssLGyvFF+/fl3OnDkjGRkZgjPf6veq2bt37/OIRHeC9uXLl1pox44de4rfFi5cuLWkpETE399/kzoA69ft2LFjPsL5gaAuXbrkXr161ZOK379/r1UB05Obm/vJpEmTVgUFBYWOHz9+zsSJE39AlLJ5Bp4+z8rKGsgzrAh46sv1Fi1alGVnZ7vZPn/+fAEE9WD44MWIXr16XYOg+NTU1Akq5EiHG4SMCgwM/BWCqghGe3t75rECZy+6urpeQ/hy+vbtexGgioZx3jdu3PADZj6dPn16PES8QWpKoHAY0tOdJYYqMNXpUb5ly5YNyKPr5s2bF+vX8/PzuwH5bYqLi/muAYve0HuASIANzTMguRphngOw/lFQUOAdGRm50mw2a/t9fHzOUdbx48f7mwgcMYz169dHQgjBtkTV65o1a75GWO/369dPAB4BSUhtbW3DGeaRiHZzcyO632N/ONcJXJCTIwBI9Gu64ICXaeTIkVfFwoDSLRBsD1aKWLt2bcjUqVNj9GVlHGAsrYyQdzl//jyV/wXSyAaQ3DMzM31oJPHBAaKpMSMvl5GffzBcLHj+48mTJ/t26NDhNsNIhLKOaQBrV70rz5HDBppluSEKpXxHHbvMnj1bpk2bVpiYmHgP0fjNDBSWjBgxIhlx/9KoGKGrgBclrF2G8v79+9o6hTdt2lRat26tpaF9+/bCPOojwt/41ozvgwYNeg00y8CBAzNXrFjRddSoUWJmiGbNmrXXkmIg3hW8OwRlcoLeGrmboCLxoxI0cOkHUY+mEou6z1q+fHkODeF+RodPefr0qbx48YIFftISGfTo0eMO0uCsEKwmDz948ID1qVGlkcc5k5OTBbwgqv5pPLAjt27dEhMtY74AoAg8q41e3759u9vcuXMPQpEd+FbLHZ9EKUuLwo4dOyaocwHtCiIkYDrNOHrHp2oMH0RM9UfE/zri/7MlxCYlJQVNmTLlEIjDrJqE8pIhZLt89OiRPHv2TEiHMFZAQBq7KeCpcwoHJBAtX9yA1rYBNZplSTn69HR4Hg3BJtVO9X2ZBrAp8J24YURwGRAYrHnLbxzEAn+beICTQuB91YEDB+aBBMotKQcZzAoNDY2GUhtj6PRtk1HkdHZ2FvC8ZgzXUb8ydOhQrWWa9GCg1yj6vOjo6LkwptaSYNRk8KJFi6IZKnpmRDrXEhISwmNiYqKQXwdlDOXzOXr0aEF7FE2ZcXLTnj17wiyhXM0xY8YcRR6d0Ai4l/yrsRYi8pPaA8JIBAfYGW8yWrSMC/orDqC/2ppykH7u7t27B6DRC+5XgjRNM+5B241HuZqNyi0q1itHs19nTTkYrBIVEefr65u0ffv20ODg4L3GPTNnzowD8m31yq0qJkk8fPhQwDxWPVcT+a0EPqaCCfcbv40bNy4GJWhqqGlritnwmbd79+6RYEIAuJqPKQdyK/ft2zcZymOM31COMWQwrZatKSbr8ILPrnPz5k3ZtWuXf6tWrZ5+TDnC/wqXgc8xYo3f0GYXXblyRUzyHweZCl0mdf/+/YOQ0xRre4Fkx7CwsOgJEyYkANkJuo5VjzZcjH8Y/rvHly5d0hoCOTktLc0GeV+Aa06JNc/RVl/ExcWNwzUokb83btz4hQKtVQYiuEj4pDnmmzXOdXIziQLc3DI+Pj4Ml8RvcKfysCQHYa/YunXrYk9PzwJ3d/cMtlCeNcv/GOpiDyCVTZ48eQNyGXX58uWxuFkGoVX2Qht1A4jMvIXyXo6rbiH+h8pQFwmOfwUYADpAWEMl4evxAAAAAElFTkSuQmCC"/>
                            </defs>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12">
                    <div class="product-support d-flex p-4 align-items-center">
                        <div class="product-support__Left">
                            <div class="product-support__Title">
                                info@lotus-climat.ru
                            </div>
                            <div class="product-support__Text mt-2">
                                Остались вопросы? Напишите нам и мы поможем найти ответ на любой вопрос
                            </div>
                        </div>
                        <div class="product-support__Right">
                            <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <circle cx="30" cy="30" r="29.5" fill="#F2F2F2" stroke="black"/>
                            <rect x="15" y="15" width="30" height="30" fill="url(#patternEmail)"/>
                            <defs>
                            <pattern id="patternEmail" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#imageEmail" transform="scale(0.0333333)"/>
                            </pattern>
                            <image id="imageEmail" width="30" height="30" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAANmSURBVHjarJdraM5xFMf/e/ZMY8PYVo9LLm1i5s4saXjBC5JLTBRvhiJJZCVN3iB54RWZkpREZN4gSlIuc8ttJDxbw6yxzOwxl13M9+j71+n0/z/7b3Pq057f/3c5v9/5nXN+ZwlOMOkFJoCZIA9kgwhIBu3gK6gBr8B9UA6iTg9lCbgAmkFHQN6DI2BqdxSOBGfBry4otNSBYpBgF0/wUToXlIIs810WqwQvQTX4BsKgPzeazb9WToBN4Hu8ky4Dnz12fwmsAkM6sdIacMVj/nGQ5MQ5acxM+ASKQJ8uXFMK2ECn02tt8xo8jGbUA1+D/B445jzQoNb7CMbrAYngtFH6Dkz0WCxEb5UrWQlmMdzcdVJonRR+kzE/1boHtW/JzlpUp8TmIg+lC8EN7rwV/AZfwG0wDSwGzxRFnFem1n4Dhrq7PGlOe9jD+0uozC90xEL3zLfVnL/czJUD/A2ZGvWxHuQaxVu6EcNi3imcPwJUqb4d8nGpmXCK9+hKnk/WqqOJoz6Kn4NBKuU+VH2loiDHnK6cd+c6UrFHKJ3hhmaDyXQYK1HmA4f+06j60kMm0/ygSVwZB+aYBa+B9czHbYz7Ep5QSyUVemXJsChOVx+aGXfazBmqLd6+n6nSMRt+Yr5VqN+pIFO1m0K0ud6V3lmWab8Fd3ySRbL63cgTu5JjUm1VSN2DO7mvavfzOFmLh9IBYLpqN5grk1Q8kL/Ff+6G+Mq4Ik40WLVbPR6BHA/FG5lyXZGYrnUdCaxQfY+kWAixatCmzjUO4piNHaCSMBcVR9tpxjWrK5SHYZLqO8Zs54xmTLoxJuGUxkG2Tz8e53jffgXAZnCInq+f1n9XKTs/rzrbTJ7eHTBTVRO//qdguL2jQl66O+iW8tIwzRNP6Qumx0KfLCeFwSivUJDjXzeD96l+qRy28hGI0bslp98Eu0yoLGDavcqSRyqS3vEe7QIu6CpuZZ2kJZMPuaTJMQyjeFVIYNluTi3m32uy23+RRNN+wPQ2Q4VXAUlkYmjsZM00WmM+WMdUGgtS3soTtoevkpVqVd7WMwKSuNkMxneEWSqiqtayrlhjLXNzRw85GsTUWh6DyzxVpBNH8pMmPpcXg/4n4VWo5/NtHsuNpLJQaGeZE6MPfOADUcErqXVTpJY/AgwABfpf2zDHOM4AAAAASUVORK5CYII="/>
                            </defs>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-tabs-wrap">
        <div class="tabs-card" data-tabs="card">
            <ul class="no-list">
                <li><a href="#card-tab-1" class="js-tab-open">Описание</a></li>
                <li class="active"><a href="#card-tab-2" name="map-tab" id="map-tab" class="js-tab-open">Характеристики</a></li>
            </ul>
        </div>
        <div class="tabs-card-content-wrap">
            <div class="row">
                <div class="col-12">
                    <div class="tabs-card-content" data-tabs-content="card">
                        <div class="tab-content" id="card-tab-1">
                            <div class="accordion-item">
                                <div class="visible">
                                    <a href="" class="js-open-accordion">
                                        Описание
                                        <svg class="icon-svg">
                                        <use xlink:href='/storage/icons/icons.svg#arrow' /></svg>
                                    </a>
                                </div>
                                <div class="hidden">
                                    <div class="descr {{strlen($product->text) > 150 ? 'card-description' : 'description-visible-product'}}">
                                        <div class="text_block">
                                            <div class="h5">{{ $product->name }}</div>
                                            {!! $product->text !!}
                                        </div>
                                        @if(strlen($product->text) > 150)
                                        <a href="" class="link-show-more js-show-descr">
                                            <svg class="icon-svg">
                                            <use xlink:href='/storage/icons/icons.svg#arrow-long' /></svg>
                                            <span>Развернуть описание</span>
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content active" id="card-tab-2">
                            <div class="accordion-item">
                                <div class="visible">
                                    <a href="" class="js-open-accordion"> Характеристики
                                        <svg class="icon-svg"> <use xlink:href='/storage/icons/icons.svg#arrow' /></svg>
                                    </a>
                                </div>
                                <div class="hidden">
                                    <div class="descr description-visible-product">
                                        <div class="text_block">
                                            @include('product.partials.parameters')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
