{{--
<div class="col-3 d-md-none">
    <aside class="right-column">
        <div class="cat-item js-sticky" data-offset-x="20">
            <a>
                <div class="img">
                    <img src="{{ isset($drug->images[0]) ? $drug->images[0]->small : '' }}" alt="{{ $drug->name }}">
                </div>
                <div class="descr">
                    <div class="text">{{ $drug->name }}</div>
                    <div class="price-wrap">
                        <div class="price-block">
                            @if($drug->prices->store > $drug->prices->storeWithDisc)
                                <div class="new_price">от <span>{{ intPricePart($drug->prices->storeWithDisc) }}</span>,{{ fractionalPricePart($drug->prices->storeWithDisc) }}₽</div>
                                <div class="old_price"><span>{{ intPricePart($drug->prices->store) }}</span>,{{ fractionalPricePart($drug->prices->store) }}₽</div>
                            @else
                                <div class="new_price">от <span>{{ intPricePart($drug->prices->store) }}</span>,{{ fractionalPricePart($drug->prices->store) }}₽</div>
                            @endif
                        </div>
                    </div>
                </div>
            </a>
            @if(!$drug->prescription)
            <div class="count-control-wrap">
                @component('orders.count-control')
                    @slot('count', 1)
                @endcomponent
                <button class="btn">
                    <svg class="icon-svg"><use xlink:href='/img/icons/icons.svg#cart' /></svg>
                    В корзину
                </button>
            </div>
            @endif
        </div>
    </aside>
</div>
--}}
