@inject('parameterCategory', 'App\Models\Catalog\ParameterCategory')

<div class="row d-block">
@foreach ($parameterCategory->all() as $category)
    @php
        $parameters = $product->parameters($category)->get()
    @endphp

    @if (count($parameters) > 0)
        <div class="col-12 col-lg-6 float-left">
            <div class="font-weight-bold mt-2">{{ $category->name }}</div>

            <div class="mt-2">
            @foreach ($parameters as $parameter)
                <div class="row m-0 p-0 mb-1 dots">
                    <div class="col-12 p-0 field">
                        <span>{{ $parameter->name }}</span>
                    </div>
                    <div class="col-4 p-0 value">{{ $parameter->pivot->value }}</div>
                </div>
            @endforeach
            </div>
        </div>
    @endif
@endforeach
</div>
