@extends('layouts.admin')

@section('content')
<div class="container mt-4">
    <div class="card">
        <div class="card-header">
            <h3>Редактирование параметра {{ $parameter->name }}</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('parameter.update', $parameter) }}" method="post">
                <input type="hidden" name="_method" value="put">    
                @csrf

                @include('admin.parameter._form')

            </form>
        </div>
    </div>
</div>
@endsection
