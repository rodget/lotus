<div class="form-group">
    <label>Наименование</label>
    <input type="text" class="form-control" name="name" value="{{ $parameter->name ?? '' }}" placeholder="Наименование">
</div>

<div class="form-group">
    <label>Тип товара</label>
    <select name="type_id" class="form-control">
        @empty ($parameter)
        <option  value="">-- Выберите тип --</option>
        @endisset
        @foreach ($parameterTypes as $parameterType)
        <option value="{{ $parameterType->id }}"
            @if ($parameter && $parameter->type_id == $parameterType->id)
            selected=""
            @endif
            >
            {{ $parameterType->name ?? '' }}
        </option>
        @endforeach
    </select>
</div>

<button type="submit" class="btn btn-primary">Сохранить</button>
