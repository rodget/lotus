@extends('layouts.admin')

@section('content')
<div class="container mt-4">
    <div class="card">
        <div class="card-header">
            <h5 class="m-0">Новый параметр</h5>
        </div>
        <div class="card-body">
            <form action="{{ route('parameter.store') }}" method="post">
                @csrf

                @include('admin.parameter._form')

            </form>
        </div>
    </div>
</div>
@endsection
