@extends('layouts.admin')

@section('content')
<div class="container mt-4">
    <div class="card">
        <div class="card-header">
            <h5 class="m-0">Параметры товаров</h5>
        </div>
        <div class="card-body">
            <div class="d-flex justify-content-end">
                <a href="{{ route('parameter.create') }}" class="btn btn-primary">Создать параметр</a>
            </div>

            <table class="table table-striped table-sm table-hover mt-4">
                <thead class='thead-dark'>
                    <tr>
                        <th class="font-weight-normal">Наименование</th>
                        <th class="font-weight-normal">Slug</th>
                        <th class="text-center font-weight-normal">Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($parameters as $parameter)
                    <tr>
                        <td class="align-middle">{{ $parameter->name ?? '' }}</td>
                        <td class="align-middle">{{ $parameter->slug ?? '' }}</td>
                        <td class="align-middle text-center">
                            <a class="btn btn-sm btn-primary" href="{{ route('parameter.edit', $parameter) }}">Редактировать</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4">
                            <h1 class="text-center">В таблице нет записей</h1>
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
