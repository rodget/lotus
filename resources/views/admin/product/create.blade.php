@extends('layouts.admin')

@section('title', 'Новый товар')

@section('content')
@if($errors->any())
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    @foreach($errors->all() as $error)
    {{ $error }}<br/>
    @endforeach
</div>
@endif
<form action="{{ route('product.store') }}" method="post">
    @csrf

    @include('admin.product._form')

</form>
@endsection
