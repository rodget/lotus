@extends('layouts.admin')

@section('title', 'Редактирование товара ' . $product->name)

@section('content')
<form action="{{ route('product.update', $product) }}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="put">    
    @csrf

    @include('admin.product._form')

</form>
@endsection
