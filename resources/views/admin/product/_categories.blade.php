@foreach ($categories as $categoryItem)
<option value="{{ $categoryItem->id }}"
    @isset ($category->id)
        
        @if ($category->id == $categoryItem->id)
            selected=""
        @endif

    @endisset
    >
        {{ $delimeter ?? '' }}{{ $categoryItem->name ?? '' }}
        
        @isset ($categoryItem->childrens)
            @include('admin.product._categories', [
                'category' => $category ?? null,
                'categories' => $categoryItem->childrens,
                'delimeter' => ' - ' . $delimeter
            ])
        @endisset
    </option>
@endforeach
