<div class="row form-group">
    <div class="col-12">Наименование</div>
    <div class="col-12">
        <input type="text" class="form-control" id="name" name="name" value="{{ Request::old('name') ?? $product->name ?? '' }}" placeholder="Наименование товара">
    </div>
</div>

<div class="row form-group">
    <div class="col-12">Категория</div>
    <div class="col-12">
        <div class="select-wrap">
            <select id="category_id" name="category_id" class="js-select form-control" required="">
                @empty ($product)
                <option  value="">-- Выберите категорию --</option>
                @endisset
                @include('admin.product._categories', ['category' => $product->category ?? null])
            </select>
        </div>
    </div>
</div>

<div class="row form-group">
    <div class="col-12">Бренд</div>
    <div class="col-12">
        <select name="producer_id" class="js-select form-control" required="">
            @empty ($product)
            <option  value="">-- Выберите бренд --</option>
            @endisset
            @foreach ($producers as $producerItem)
            <option value="{{ $producerItem->id }}"
                    @isset ($product)

                    @if ($product->producer_id == $producerItem->id)
                selected=""
                @endif

                @endisset
                >
                {{ $producerItem->name ?? '' }}
            </option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label>Meta-title</label>
    <input type="text" class="form-control" id="title" name="title" value="{{ Request::old('title') ?? $product->title ?? '' }}" placeholder="Мета-тег title">
</div>

<div class="form-group">
    <label>Meta-keywords</label>
    <input type="text" class="form-control" id="keywords" name="keywords" value="{{ Request::old('keywords') ?? $product->keywords ?? '' }}" placeholder="Мета-тег keywords">
</div>

<div class="form-group">
    <label>Meta-description</label>
    <input type="text" class="form-control" id="description" name="description" value="{{ Request::old('description') ?? $product->description ?? '' }}" placeholder="Мета-тег description">
</div>

<div class="form-group">
    <label>Цена</label>
    <input type="text" class="form-control" id="price" name="price" value="{{ Request::old('price') ?? $product->price->price ?? '' }}" placeholder="Цена">
</div>

<div class="form-group">
    <label>Описание</label>
    <textarea class="form-control editor" name="text" id="editor">{{ Request::old('text') ?? $product->text ?? '' }}</textarea>
</div>

<div class="form-group">
    <label for="file">Изображение</label>
    <input type="file" class="form-control-file" id="file" name="file" />
</div>

<button type="submit" class="btn btn-primary">Сохранить</button>
