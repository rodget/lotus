@inject('parameterCategory', 'App\Models\Catalog\ParameterCategory')

@extends('layouts.admin')

@section('title', 'Редактирование параметров товара ' . $product->name)

@section('content')

<form action="{{ route('product.parameters.store', $product) }}" method="post" enctype="multipart/form-data">
    @csrf

    @foreach ($parameterCategory->all() as $category)
    @php
    $parameters = $product->category->parameters($category)->get()
    @endphp
    @if (count($parameters) > 0)
    <h5>{{ $category->name }}</h5>

    @foreach ($parameters as $parameter)
    @if ($parameter->type_id == 1)
    <div class="row ml-4">
        <label class="col-sm-4 col-form-label" for="parameter{{ $parameter->id }}">{{ $parameter->name }}</label>
        <div class="col-sm-8">
            <input type="text" class="form-control form-control-sm" name="parameters[{{ $parameter->id }}]" id="parameter{{ $parameter->id }}"
                   @if ($product->parameters->contains($parameter))
                   value="{{ $product->parameters->find($parameter->id)->pivot->value }}"
                   @endif
                   >
        </div>
    </div>
    @else if ($parameter->type_id == 2)
    <div class="form-check">
        <input class="form-check-input" type="checkbox" name="parameters[{{ $parameter->id }}]" value="{{ $parameter->id }}" id="parameter{{ $parameter->id }}"
               @if ($product->parameters->contains($parameter))
        checked=""
        @endif
        >
        <label class="form-check-label" for="parameter{{ $parameter->id }}">
            {{ $parameter->name }}
        </label>
    </div>
    @endif
    @endforeach
    @endif
    @endforeach

    <div class="d-flex mt-4 justify-content-end">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
</form>
@endsection
