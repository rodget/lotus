@extends('layouts.admin')

@section('content')
<div class="container mt-4">
    <div class="card">
        <div class="card-header">
            <h5 class="m-0">Производители</h5>
        </div>
        <div class="card-body">
            <div class="d-flex justify-content-end">
                <a href="{{ route('producer.create') }}" class="btn btn-primary">Создать бренд</a>
            </div>

            <table class="table table-striped table-sm table-hover mt-4">
                <thead class='thead-dark'>
                    <tr>
                        <th class="font-weight-normal" style="width: 40px;">Логотип</th>
                        <th class="font-weight-normal">Наименование</th>
                        <th class="text-center font-weight-normal">Активность</th>
                        <th class="text-center font-weight-normal">Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($producers as $producer)
                    <tr>
                        <td>
                            @if ($producer->logo)
                            <img src="/storage/producers/{{ $producer->logo }}" class="d-block w-100" />
                            @endif
                        </td>
                        <td class="align-middle">{{ $producer->name ?? '' }}</td>
                        <td class="align-middle text-center">{{ $producer->status ? 'Да' : 'Нет' }}</td>
                        <td class="align-middle text-center">
                            <a class="btn btn-sm btn-primary" href="{{ route('producer.edit', $producer) }}">Редактировать</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4">
                            <h1 class="text-center">Бренды отсуствуют</h1>
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
