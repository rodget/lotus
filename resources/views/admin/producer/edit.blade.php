@extends('layouts.admin')

@section('content')
<div class="container mt-4">
    <div class="card">
        <div class="card-header">
            <h3>Редактирование бренда {{ $producer->name }}</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('producer.update', $producer) }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="put">    
                @csrf

                @include('admin.producer._form')

            </form>
        </div>
    </div>
</div>
@endsection
