@extends('layouts.admin')

@section('content')
<div class="container mt-4">
    <div class="card">
        <div class="card-header">
            <h5 class="m-0">Новый бренд</h5>
        </div>
        <div class="card-body">
            <form action="{{ route('producer.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                @include('admin.producer._form')

            </form>
        </div>
    </div>
</div>
@endsection
