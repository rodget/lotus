<div class="form-group">
    <label>Наименование</label>
    <input type="text" class="form-control" name="name" value="{{ $producer->name ?? '' }}" placeholder="Наименование">
</div>

<div class="form-group">
    <label>Meta-title</label>
    <input type="text" class="form-control" name="title" value="{{ $producer->title ?? '' }}" placeholder="Мета-тег title">
</div>

<div class="form-group">
    <label>Meta-keywords</label>
    <input type="text" class="form-control" name="keywords" value="{{ $producer->keywords ?? '' }}" placeholder="Мета-тег keywords">
</div>

<div class="form-group">
    <label>Meta-description</label>
    <input type="text" class="form-control" name="description" value="{{ $producer->description ?? '' }}" placeholder="Мета-тег description">
</div>

<div class="form-group">
    <label for="file">Логотип</label>
    <input type="file" class="form-control-file" id="file" name="file" />
</div>

<button type="submit" class="btn btn-primary">Сохранить</button>
