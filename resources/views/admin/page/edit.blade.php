@extends('layouts.admin')

@section('title', 'Редактирование страницы ' . $page->name)

@section('content')

<form action="{{ route('page.update', $page) }}" method="post">
    <input type="hidden" name="_method" value="put">    
    @csrf

    @include('admin.page._form')

</form>

@endsection
