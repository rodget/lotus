<div class="form-group">
    <label>Наименование</label>
    <input type="text" class="form-control" name="name" value="{{ $page->name ?? '' }}" placeholder="Наименование">
</div>

<div class="form-group">
    <label>Meta-title</label>
    <input type="text" class="form-control" name="title" value="{{ $page->title ?? '' }}" placeholder="Мета-тег title">
</div>

<div class="form-group">
    <label>Meta-keywords</label>
    <input type="text" class="form-control" name="keywords" value="{{ $page->keywords ?? '' }}" placeholder="Мета-тег keywords">
</div>

<div class="form-group">
    <label>Meta-description</label>
    <input type="text" class="form-control" name="description" value="{{ $page->description ?? '' }}" placeholder="Мета-тег description">
</div>

<div class="form-group">
    <label>Описание</label>
    <textarea class="form-control editor" name="text" id="editor">{{ Request::old('text') ?? $page->text ?? '' }}</textarea>
</div>

<button type="submit" class="btn btn-primary">Сохранить</button>
