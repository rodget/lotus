@extends('layouts.admin')

@section('title', 'Страницы')

@section('content')
<div class="d-flex justify-content-end">
    <a href="{{ route('page.create') }}" class="btn btn-primary">Создать страницу</a>
</div>

<table class="table table-striped table-sm table-hover mt-4">
    <thead class='thead-dark'>
        <tr>
            <th class="font-weight-normal">Наименование</th>
            <th class="text-center font-weight-normal">Активность</th>
            <th class="text-center font-weight-normal">Действие</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($page as $page)
        <tr>
            <td class="align-middle">{{ $page->name ?? '' }}</td>
            <td class="align-middle text-center">{{ $page->status ? 'Да' : 'Нет' }}</td>
            <td class="align-middle text-center">
                <a class="btn btn-sm btn-primary" href="{{ route('page.edit', $page) }}">Редактировать</a>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="4">
                <h1 class="text-center">Страницы отсуствуют</h1>
            </td>
        </tr>
        @endforelse
    </tbody>
</table>

@endsection
