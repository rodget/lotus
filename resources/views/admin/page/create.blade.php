@extends('layouts.admin')

@section('title', 'Новая страница')

@section('content')

<form action="{{ route('page.store') }}" method="post">
    @csrf

    @include('admin.page._form')

</form>

@endsection
