@extends('layouts.admin')

@section('title', 'Категории')

@section('content')

<div class="d-flex justify-content-end">
    <a href="{{ route('category.create') }}" class="btn btn-primary">Создать категорию</a>
</div>

<table class="table table-striped table-sm table-hover mt-4">
    <thead class='thead-dark'>
        <tr>
            <th class="font-weight-normal">Наименование</th>
            <th class="text-center font-weight-normal">Товаров</th>
            <th class="text-center font-weight-normal">Активность</th>
            <th class="text-center font-weight-normal">Действие</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($categories as $category)
        <tr>
            <td class="align-middle">{{ $category->name ?? '' }}</td>
            <td class="align-middle text-center">
                <a href="{{ route('category.products', $category) }}">{{ count($category->products) }}</a>
            </td>
            <td class="align-middle text-center">{{ $category->status ? 'Да' : 'Нет' }}</td>
            <td class="align-middle text-center" style="width: 80px;">
                <a class="btn btn-sm btn-primary w-100" href="{{ route('category.edit', $category) }}">Редактировать</a>
                <a class="btn btn-sm btn-primary w-100" href="{{ route('category.parameters', $category) }}">Параметры</a>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="4">
                <h1 class="text-center">Категории отсуствуют</h1>
            </td>
        </tr>
        @endforelse
    </tbody>
</table>


@endsection
