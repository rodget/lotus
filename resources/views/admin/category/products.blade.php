@extends('layouts.admin')

@section('title', $category->name)

@section('content')

<div class="d-flex justify-content-end">
    <a href="{{ route('product.create') }}" class="btn btn-primary">Создать товар</a>
</div>

<div class="row mt-4">
    <div class="col-12 d-flex justify-content-end">
        {{ $products->links() }}
    </div>
</div>
<table class="table table-striped table-sm table-hover mt-4">
    <thead class='thead-dark'>
        <tr>
            <th class="font-weight-normal" style="width: 40px;">Фото</th>
            <th class="font-weight-normal">Наименование</th>
            <th class="font-weight-normal">Бренд</th>
            <th class="text-center font-weight-normal">Цена</th>
            <th class="text-center font-weight-normal">Активность</th>
            <th class="text-center font-weight-normal">Действие</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($products as $product)
        <tr>
            <td class="align-middle">
                @if ($product->image)
                <img src="/storage/products/{{ $product->image->path }}" class="d-block w-100" />
                @endif
            </td>
            <td class="align-middle">{{ $product->name ?? '' }}</td>
            <td class="align-middle">{{ $product->producer->name ?? '' }}</td>
            <td class="align-middle text-center">{{ $product->price->price ?? '' }}</td>
            <td class="align-middle text-center">{{ $product->status ? 'Да' : 'Нет' }}</td>
            <td class="align-middle text-center" style="width: 80px;">
                <a class="btn btn-sm btn-primary w-100" href="{{ route('product.edit', $product) }}">Редактировать</a>
                <a class="btn btn-sm btn-primary w-100" href="{{ route('product.parameters', $product) }}">Параметры</a>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="5">
                <h3 class="text-center">Товары отсуствуют</h3>
            </td>
        </tr>
        @endforelse
    </tbody>
</table>
<div class="row mt-4">
    <div class="col-12 d-flex justify-content-end">
        {{ $products->links() }}
    </div>
</div>

@endsection
