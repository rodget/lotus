@foreach ($categories as $categoryItem)
    <option value="{{ $categoryItem->id }}"
    @isset ($category->id)
        
        @if ($category->parent_id == $categoryItem->id)
            selected=""
        @endif
        
        @if ($category->id == $categoryItem->id)
            disabled="1"
        @endif
    @endisset
    >
        {{ $delimeter ?? '' }}{{ $categoryItem->name ?? '' }}
        
        @isset ($categoryItem->childrens)
            @include('admin.category._categories', [
                'categories' => $categoryItem->childrens,
                'delimeter' => ' - ' . $delimeter
            ])
        @endisset
    </option>
@endforeach
