<div class="form-group">
    <label>Наименование</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ $category->name ?? '' }}" placeholder="Наименование категории">
</div>

<div class="form-group">
    <label>Родительская категория</label>
    <select name="parent_id" class="form-control">
        <option value="0">-- Без родительской категории --</option>
        @include('admin.category._categories')
    </select>
</div>

<div class="form-group">
    <label>Meta-title</label>
    <input type="text" class="form-control" name="title" value="{{ $category->title ?? '' }}" placeholder="Мета-тег title">
</div>

<div class="form-group">
    <label>Meta-keywords</label>
    <input type="text" class="form-control" name="keywords" value="{{ $category->keywords ?? '' }}" placeholder="Мета-тег keywords">
</div>

<div class="form-group">
    <label>Meta-description</label>
    <input type="text" class="form-control" name="description" value="{{ $category->description ?? '' }}" placeholder="Мета-тег description">
</div>


<div class="form-group">
    <label>Иконка</label>
    <textarea class="form-control" name="icon" rows="3">{{ $category->icon ?? '' }}</textarea>
</div>
<button type="submit" class="btn btn-primary">Сохранить</button>
