@extends('layouts.admin')

@section('content')
<div class="container mt-4">
    <div class="card">
        <div class="card-header">
            <h5 class="m-0">Параметры категории {{ $category->name }}</h5>
        </div>
        <div class="card-body">
            <form action="{{ route('category.parameters.store', $category) }}" method="post">
                @csrf

                @foreach ($parameters as $parameter)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="parameters[{{ $parameter->id }}]" value="{{ $parameter->id }}" id="parameter{{ $parameter->id }}"
                           @if ($category->parameters && in_array($parameter->id, json_decode($category->parameters, true)))
                           checked=""
                           @endif
                           >
                    <label class="form-check-label" for="parameter{{ $parameter->id }}">
                        {{ $parameter->name }}
                    </label>
                </div>
                @endforeach

                <button type="submit" class="btn btn-primary">Сохранить</button>

            </form>
        </div>
    </div>
</div>
@endsection
