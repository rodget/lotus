@extends('layouts.admin')

@section('content')
<div class="container">
    <form action="{{ route('category.store') }}" method="post">
        @csrf
        
        @include('admin.category._form')
        
    </form>
</div>
@endsection
