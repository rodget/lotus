@extends('layouts.admin')

@section('title', 'Редактирование категории ' . $category->name)

@section('content')

<form action="{{ route('category.update', $category) }}" method="post">
    <input type="hidden" name="_method" value="put">    
    @csrf

    @include('admin.category._form')

</form>

@endsection
