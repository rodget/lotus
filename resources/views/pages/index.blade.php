@extends('layouts.index')

@section('content')
<div>
    <img src="/storage/banners/001.jpg" class="d-block w-100" />
</div>

<div class="container page-index__slogan mt-4">
    <div class="row">
        <div class="col-12"><h1>Интернет-магазин климатической техники</h1></div>
    </div>
    <div class="row mt-4">
        <div class="col-12">
            <p>
                Всё для Вас и Вашей семьи для создания приятного климата и комфорта в Вашем доме. Профессионализм и многолетний опыт наших специалистов гарантирует правильный выбор климатической техники для Вас.
                <br /><br />
                Мы ждем ваших заказов!
            </p>
        </div>
    </div>
</div>

<div class="container page-index__motto">
    <div class="row mt-4">
        <div class="col-12 col-md-6 text-center motto-image">
            <img src="/storage/images/motto-001.png" />
        </div>
        <div class="col-12 col-md-6 d-flex align-items-center mt-2 mt-md-0">
            <div>
                <div class="text-left motto-title">Каждый клиент - это победа!</div>
                <div class="text-left motto-text mt-4">"Клиент – это самый важный посетитель. Не он зависит от нас. Мы зависим от него. Он не прерывает нашу работу. Он – цель нашей работы. Он не по ту сторону нашего бизнеса. Он – его часть"</div>
                <div class="text-right motto-sign mt-2">- Махатма Ганди</div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-12 col-md-6 d-flex align-items-center mt-2 mt-md-0">
            <div>
                <div class="text-right motto-title">Будет стоять как надо</div>
                <div class="text-right motto-text mt-4">Срок службы техники зависит от квалифицированной установки и своевременного сервисного обслуживания</div>
            </div>
        </div>
        <div class="col-12 col-md-6 text-center motto-image">
            <img src="/storage/images/motto-002.png" />
        </div>
    </div>
</div>
@endsection
