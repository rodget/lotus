@extends('layouts.index')

@section('content')

<div class="container mt-4">
    <div class="row">
        <div class="col-12"><h1>{{ $page->name }}</h1></div>
    </div>
    <div class="row mt-4">
        <div class="col-12">
            {!! $page->text !!}
        </div>
    </div>
</div>

@endsection

