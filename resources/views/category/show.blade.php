@extends('layouts.index')

@section('breadcrumbs')
    {{ Breadcrumbs::render('category', $category) }}
@endsection

@section('content')
<div class="container category">
    <div class="row mt-4">
        <div class="col-12">
            <h1 class="h1">{{ $category->name }}{{ $producer ? ' ' . $producer->name : ''}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-5 col-md-3 col-lg-3">
            <div class="filter-column p-3 mt-4">
                <div class="filter-title">Бренд</div>
                @foreach ($producers as $producer)
                <div class="filter-item mx-4 mt-2">
                    <a href="{{ route('category.producer', [$category->slug, $producer->slug]) }}" class="text-muted">{{ $producer->name }}</a>
                </div>
                @endforeach

                <div class="filter-title mt-4">Цена</div>
                <div class="filter-item mx-4 mt-4">
                    <div id="sliderPrice"></div>
                </div>
                
                <div class="filter-title mt-4">Площадь, м<sup>2</sup></div>
                <div class="filter-item mx-4 mt-4">
                    <div id="sliderArea"></div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-7 col-md-9 col-lg-9">
            <div class="row categoryList">
                @foreach ($products as $product)
                <div class="col-12 col-md-6 col-lg-4 mt-4 category-item">
                    <div class="card h-100 border-0 shadow bg-white categoryList__item">
                        <div class="card-body pb-2">
                            <div>
                                <a href="{{ route('product.show', $product->slug) }}">
                                    @if ($product->image)
                                        <img src="/storage/products/{{ $product->image->path }}" class="d-block w-100" />
                                    @else
                                        <img src="/storage/images/no_image.jpg" class="d-block w-100" />
                                    @endif
                                </a>
                            </div>
                            <div class="categoryList__name">
                                <a href="{{ route('product.show', $product->slug) }}">{{ $product->name }}</a>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent border-0 pt-0">
                            @php
                                $parameters = $product->parameters(null, ['is_list' => 1])->get()
                            @endphp

                            @if (count($parameters) > 0)
                                @foreach ($parameters as $parameter)
                                    <div class="row m-0 categoryList__parameters dots">
                                        <div class="col-12 p-0 field">
                                            <span>{{ $parameter->name }}</span>
                                        </div>
                                        <div class="col-4 p-0 value">{{ $parameter->pivot->value }}</div>
                                    </div>
                                @endforeach
                            @endif
                            
                            <div class="row">
                                <div class="col-12">
                                    <hr />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-7 d-flex align-items-center categoryList__price">
                                    {{ $product->price ? number_format($product->price->price, 0, '.', ' ') : ''}} р.
                                </div>
                                <div class="col-5 d-flex align-items-center">
                                    <span class="btn link-cart" v-on:click="addCart({{ $product->id }})">
                                        <svg class="icon-svg">
                                            <use xlink:href='/storage/icons/icons.svg#cart' /></svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row mt-4">
                <div class="col-12 d-flex justify-content-end">
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
