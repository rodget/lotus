<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-9">
                    <div class="header-contacts">
                        <address>
                            <svg class="icon-svg">
                                <use xlink:href='/storage/icons/icons.svg#pin' /></svg>
                            г. Москва, ул. Академика Королева, д. 13, стр. 1
                         </address>
                        <div class="header-phone">
                            <a href="tel:+74952217727" class="tel">
                                <svg class="icon-svg">
                                    <use xlink:href='/storage/icons/icons.svg#tube' /></svg>
                                +7 (495) 221 77 27
                            </a>
                        </div>
                        <div class="open-time">
                            <svg class="icon-svg">
                                <use xlink:href='/storage/icons/icons.svg#clock' /></svg>
                            Пн-Пт: 10:00-18:00
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="header-auth">
                        @if (auth()->check())
                        <a href="{{ route('personal') }}" class="link">
                            <svg class="icon-svg">
                                <use xlink:href='/storage/icons/icons.svg#user' /></svg>
                            <span>{{ auth()->user()->name }}</span>
                        </a>
                        @else
                        <a href="" class="link" data-click="auth">
                            <svg class="icon-svg">
                                <use xlink:href='/storage/icons/icons.svg#user' /></svg>
                            <span>Вход</span>
                        </a>
                        |
                        <a href="" class="link" data-click="reg"><span>Регистрация</span></a>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row header-primary d-flex align-items-center">
            <div class="header-primary__logo col-12 col-xl-3">
                @if(!activeRoutes([route('home')]))
                <a href="{{ route('home') }}"><img src="/storage/images/logo.png"></a>
                @else
                <img src="/storage/images/logo.png">
                @endif
            </div>
            <div class="col-12 col-xl-9">
                <div class="header-primary__top">
                    <div class="row">
                        <div class="col-6 d-flex d-xl-block">
                            <div class="open-menu js-open-menu">
                                <div class="open-menu-box">
                                    <div class="open-menu-inner"></div>
                                </div>
                            </div>
                            <div class="form-search" id="header-search-block">
                                <a href="" class="link-search js-open-search">
                                    <svg class="icon-svg">
                                    <use xlink:href='/storage/icons/icons.svg#search' />
                                    </svg>
                                </a>
                                <form action="">
                                    <div class="input-field">
                                        <div class="input">
                                            <input type="search" class="form-control js-search-popup header-search-input" placeholder="Поиск">
                                            <button class="go-to-search">
                                                <svg class="icon-svg">
                                                <use xlink:href='/storage/icons/icons.svg#search' />
                                                </svg>
                                            </button>
                                            <a class="link-close js-close-search">
                                                <svg class="icon-svg">
                                                <use xlink:href='/storage/icons/icons.svg#close' /></svg>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                                <div class="form-search-results"></div>
                            </div>
                            <div class="header-phone-mobile">
                                <address>
                                    <a href="tel:+74952217727" class="tel">+7 (495) 221 77 27</a>
                                </address>
                            </div>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end">
                            <cart ref="cart"></cart>
                        </div>
                    </div>
                </div>
                <div class="header-primary__bottom">
                    <nav class="header-primary__link">
                        <ul>
                            <li><a href="" class="js-open-menu">Каталог товаров</a></li>
                            <li><a href="/o-kompanii">О компании</a></li>
                            <li><a href="/kontakty">Контакты</a></li>
                            <li><a href="/dostavka">Доставка</a></li>
                            <li><a href="/promos">Акции</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
