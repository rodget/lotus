@if (count($breadcrumbs))
    <nav class="breadcrumbs">
        <div class="container">
            <ul class="no-list">
                @foreach ($breadcrumbs as $breadcrumb)
                    @if ($breadcrumb->url && !$loop->last)
                        <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                    @else
                        <li><span>{{ $breadcrumb->title }}</span></li>
                    @endif
                @endforeach
            </ul>
        </div>
    </nav>
@endif



