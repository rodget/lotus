<div class="bg-lotus-grey mt-4">
    <div class="container">
        <div class="row footer-primary border-light border-bottom">
            <div class="col-12 col-md-4 align-items-center">
                <div class="footer-primary__logo">
                    <img src="/storage/images/logo-white.png" />
                </div>
            </div>
            <div class="col-12 col-md-4"></div>
            <div class="col-12 col-md-4"></div>
        </div>
        <div class="row footer-second">
            <div class="col-12 col-md-4">
                <div class="footer-second__title">Каталог</div>
                @foreach ($categories as $category)

                    <div class="py-1">
                        <a class="text-light pl-2" href="{{ route('category.show', $category->slug) }}">{{ $category->name }}</a>
                    </div>

                @endforeach
            </div>
            <div class="col-12 col-md-4">
                <div class="footer-second__title">Покупателям</div>
                <div class="py-1">
                    <a class="text-light pl-2" href="/o-kompanii">О компании</a>
                </div>
                <div class="py-1">
                    <a class="text-light pl-2" href="/kontakty">Контакты</a>
                </div>
                <div class="py-1">
                    <a class="text-light pl-2" href="/dostavka">Доставка</a>
                </div>
                <div class="py-1">
                    <a class="text-light pl-2" href="/promos">Акции</a>
                </div>
            </div>
            <div class="col-12 col-md-4"></div>
        </div>
    </div>
</div>
