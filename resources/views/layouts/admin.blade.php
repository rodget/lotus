<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@if(isset($page->meta_title)){{ $page->meta_title }} @elseif(isset($page->title)) {{ $page->title }} @endif</title>
        @if(isset($page->meta_description))
        <meta name="description" content="{{ $page->meta_description }}">
        @endif
        @if(isset($page->meta_keywords))
        <meta name="keywords" content="{{ $page->meta_keywords }}">
        @endif

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>
    <body>
        <div id="app">
            @include('layouts.admin.header')

            <div class="container my-4">
                <div class="card">
                    <div class="card-header">
                        <h3>@yield('title')</h3>
                    </div>
                    <div class="card-body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('/js/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ mix('/js/app.js') }}"></script>

        <script>
            if (document.getElementById('editor')) {
                var editor = CKEDITOR.replace('editor');
            }
        </script>
    </body>
</html>
