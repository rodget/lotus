<div class="pop" id="pop-auth">
    <div class="title-form">
        <div class="h4">Вход</div>
    </div>
    <form id="auth__form" novalidate>
        <div class="input-field">
            <div class="input">
                <input required type="text" id="auth__login" class="form-control" placeholder="E-mail">
                <div class="error-msg" id="auth__login-error">Неверный email</div>
            </div>
        </div>
        <div class="input-field">
            <div class="input">
                <input required type="password" id="auth__password" class="form-control" placeholder="Пароль">
                <div class="error-msg" id="auth__password-error">Неверный пароль</div>
            </div>
        </div>
        <div class="input-field">
            <div class="form-bottom">
                <button type="submit" class="btn btn-lotus" data-click="doAuth">Войти</button>
                <a href="" class="link">Забыли пароль?</a>
            </div>
        </div>
    </form>
    <div class="pop-bottom">
        <div class="text">Нет аккаунта?</div>
        <a href="" class="btn -border">Зарегистрироваться</a>
    </div>
</div>
