<div class="pop-menu pop-menu-main" id="pop-menu-categories">
    <div class="pop-wrap">
        <div class="container">
            <nav class="menu-pop">
                <ul class="no-list">
                    @foreach ($categories as $category)
                    <li>
                        <a href="{{ route('category.show', $category->slug) }}">
                            {{ $category->name }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </nav>
        </div>
    </div>
</div>
