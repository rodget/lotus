<nav class="navbar navbar-expand-lg navbar-dark bg-lotus-dark">
  <a class="navbar-brand" href="{{ route('home') }}">Logo</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          Каталог
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ route('category.index') }}">Категории</a>
          <a class="dropdown-item" href="{{ route('producer.index') }}">Производители</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('parameter.index') }}">Параметры</a>
          <a class="dropdown-item" href="#">Статистика</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Склад</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Торговля</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Настройки
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ route('page.index') }}">Страницы</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Пользователи</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Введите текст..." aria-label="Search">
      <button class="btn btn-lotus my-2 my-sm-0" type="submit">Найти</button>
    </form>
  </div>
</nav>
