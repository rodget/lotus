<nav class="navbar navbar-expand p-0">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav w-100">
            <li class="nav-item dropdown w-100">
                <span class="nav-link w-100 p-0 d-flex align-items-center justify-content-center border-0 text-light cursor-pointer" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div>
                        <div class="w-100 text-center">
                            <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <rect width="40" height="40" fill="url(#pattern0)"/>
                                <defs>
                                    <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                        <use xlink:href="#image0" transform="scale(0.025)"/>
                                    </pattern>
                                    <image id="image0" width="40" height="40" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAARySURBVHja5Jh5iFdVFMefzqhjZmpRmmbUGFmZW1pZDkKaaI4gWtlGFLbQtFkRaWCi/tdKUUIoaWWIlgthu9kCuWUlZYapaWJhmejY5Ew5v/HX58j31eNx7/39Zn6/mQY68JnlvXfPO/fcs9x322Sz2ag1S9uolUtpgeO7wUzoBV/COvgWDhXLwDYFLvFkWGJ6oBZ+gLXwGWyEPVD/Xxo4F+723KuDrbBJBtvvXdDQkkscellHGCqq5OFt8AV8Al/BDjjWnB5cDpP09yJ5aiQMgfIcSVijkFiv2DXDd0KmWAZ2hu/gDP1/JazR353gfLgYKuBSOBPaBfTVyaOfwwp49/hVM7CJzMj+K3uhc+DZjjAUquB12AHHsmGpPO68Jhp3JxxNKHuukeNPhCFwLyyB7Q4DZzTWwPYwFlalFB2AswpYiUjeHwzvJPQ+YvfyyeLecD3cCIMcgX4L/FhgNTA9m6E6ce1IrjIzHKbABDjFcf9jmK6gLpacnUqayBXM18EH0OCIi3otwwQoKXBZ03RSssUyLhmD3eA++MaTUfthrjIxaiaqEu87DL1jA6+AnQ6jamAtPAC9mtEwS5CHoDbx7sXxfftxyOO19+CiZjTsVBm2LfXeX6E8aeDiQLE8AutgJgyDdkUwzJbuMdjjeN9PMCL5fKRgvxZe8QyKpUEx+iyMga6NNOxCFfT9HkcsgD7pcWklXWC0jNiaoxXtgoUwGXoGDDPPL5IRaamGF6Cfo9MMhA6hGZfBZTAH1sNfAWN/gRVwh7zQXl5+EzKO5/fB4w6P2USnwfdasTfs4iWaxYPQH9o6jC3RLKcqeQ4HjK3WZsAl1nOnOzx+LjyhiSblaKTA/OeCkmK2lqbM413rvbfCUodSl2yC2+CklB6rEvM8E7Yy97A9tDmg+Gt4RnHZxWNsd3WW+aldiW2nPoSrteTJMSO17frTEy5Pw3lxkpwu927J4YXdyrRroEeg6I6Ce6Aida8UJsJqj35rFo/KHmcWl2lZZ8EGqAsY+xusVFKU59Epbtcy+1bpLkfZKsu1HxygxFkd6DYmf8BHyr5BiU1ETyXEds+4T+EGR5wPh1dVc2fbhZNVqCu0m3EZa16aolKyL2BsRp56y5M8GZWeMY4qUalxyV1Uxm5uTFywvvi8FPi+MU6DSfCSinU+UqNONSyl6wS4WZXDJa9FqT1YOiniTtEj8G1hGf6UYqneUZCfjDMyga3a/YHEtF3UTVaT7eG+8HZqu5N17DBWKtj7eIwt1YfQNGX71HRGqn7O8fT8jJa4MtkskoP7Sqklxe8BY62ortE+cUCeG4X+6lYHHfpqtaO63DU2CnQKq/zLVVJ8Uqc+PUvx1SGlp0Ivd5Wsg9qlDwxNLp+The4wAq6CUTohcIkp2gLvw88wVqRlL7wMC2F3sU+3uuoYY7zOYC5oxFg7OJqnM5wDLXH8ZqdXg+Wl0Towcp29bIAXYVn8rduS54OxlEA/edVC4RydtM7XIVBDUxUXy8D/7yF6qzfwbwEGACnGuFxeaxOFAAAAAElFTkSuQmCC"/>
                                </defs>
                            </svg>
                        </div>
                        <div class="w-100 text-center">Каталог товаров</div>
                    </div>
                </span>
                <div class="dropdown-menu m-0 p-0 rounded-0 border-0" aria-labelledby="navbarDropdown">
                    @foreach ($categories as $category)

                    <a class="dropdown-item d-flex align-items-center" href="{{ route('category.show', $category->slug) }}"><span class="mr-4">{!! $category->icon !!}</span> {{ $category->name }}</a>

                    @endforeach
                </div>
            </li>
        </ul>
    </div>
</nav>
