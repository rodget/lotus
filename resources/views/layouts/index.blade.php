<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9, user-scalable=0">

        <title>@if(isset($page->meta_title)){{ $page->meta_title }} @elseif(isset($page->title)) {{ $page->title }} @endif</title>
        @if(isset($page->meta_description))
        <meta name="description" content="{{ $page->meta_description }}">
        @endif
        @if(isset($page->meta_keywords))
        <meta name="keywords" content="{{ $page->meta_keywords }}">
        @endif

        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        
        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>
    <body>
        <div id="app">
            @include('layouts.partials.header')

            @yield('breadcrumbs')
            @yield('content')

            @include('layouts.footer')
            @include('layouts.partials.modals')
        </div>
        
        <script src="{{ mix('/js/app.js') }}"></script>
        <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(65197123, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/65197123" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
    </body>
</html>
