@extends('layouts.index')

@section('content')
<div class="container category">
    <div class="row mt-4">
        <div class="col-12">
            <h1 class="h1">Личный кабинет</h1>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-3">
            @if (auth()->user()->type == 'admin')
            <div class="py-2 border-bottom border-muted">
                <a href="{{ route('admin') }}" class="text-muted">Панель управления</a>
            </div>
            @endif
            <div class="py-2 border-bottom border-muted">
                <a href="{{ route('logout') }}" class="text-muted">Выход</a>
            </div>
        </div>
        <div class="col-9">
        </div>
    </div>
</div>
@endsection
